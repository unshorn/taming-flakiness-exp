#!/bin/bash
PROG=jsoup


mkdir tmp
mkdir results/$PROG
rm -rf tmp/*
cp dataset/$PROG-saflated.zip mnt/
cd mnt/
unzip $PROG-saflated.zip
mv $PROG $PROG-saflated
cp ../dataset/$PROG.zip .
unzip $PROG.zip
rm $PROG.zip
rm $PROG-saflated.zip
cd ..

counter=1
until [ $counter -gt 10 ]
do

  output=testresults-network-$PROG-networkon-saflateoff-${counter}.zip

  docker run --rm -e DISPLAY=$DISPLAY -v `pwd`/mnt:/mnt:z --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" saflatexp /bin/bash -c "mvn -Dmaven.repo.local=/mnt/cache clean test -f /mnt/$PROG/pom.xml -fn"
  rm -rf tmp/*
  cd mnt/$PROG
  
  find . -type f -regex '.*TEST.*.xml'  -exec cp "{}" ../../tmp  \;

  cd ../../tmp
  zip -r ../results/$PROG/$output ./


  cd ..

  output=testresults-network-jsoup-networkon-saflateon-${counter}.zip

  docker run --rm -e DISPLAY=$DISPLAY -v `pwd`/mnt:/mnt:z --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" saflatexp /bin/bash -c "mvn -Dmaven.repo.local=/mnt/cache clean test -f /mnt/$PROG-saflated/pom.xml -fn -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true"
  rm -rf tmp/*
  cd mnt/$PROG-saflated

  find . -type f -regex '.*TEST.*.xml'  -exec cp "{}" ../../tmp  \;

  cd ../../tmp
  zip -r ../results/$PROG/$output ./


  cd ..


 output=testresults-network-jsoup-networkoff-saflateon-${counter}.zip

  docker run --rm --network none -e DISPLAY=$DISPLAY -v `pwd`/mnt:/mnt:z --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" saflatexp /bin/bash -c "mvn -Dmaven.repo.local=/mnt/cache clean test -f /mnt/$PROG-saflated/pom.xml -fn -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true"
  rm -rf tmp/*
  cd mnt/$PROG-saflated

  find . -type f -regex '.*TEST.*.xml'  -exec cp "{}" ../../tmp  \;

  cd ../../tmp
  zip -r ../results/$PROG/$output ./


  cd ..

 output=testresults-network-jsoup-networkoff-saflateoff-${counter}.zip

  docker run --rm --network none -e DISPLAY=$DISPLAY -v `pwd`/mnt:/mnt:z --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" saflatexp /bin/bash -c "mvn -o -Dmaven.repo.local=/mnt/cache clean test -f /mnt/$PROG/pom.xml -fn"
  rm -rf tmp/*
  cd mnt/$PROG

  find . -type f -regex '.*TEST.*.xml'  -exec cp "{}" ../../tmp  \;

  cd ../../tmp
  zip -r ../results/$PROG/$output ./


  cd ..

  ((counter++))

done

echo All done
