package nz.ac.wgtn.ecs.saflate.analyse;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import com.google.common.io.MoreFiles;
import com.google.common.io.RecursiveDeleteOption;
import com.google.common.math.Stats;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Executable to analyse surefire reports with different configurations.
 * Folders need to have the following name: testresults-<experimenttype>-<program>-<configuration>-<saflateoff|saflateoff>-counter.zip
 * @author jens dietrich
 */
public class Main {

    public static final String ROOT_SURFIRE_REPORTS = "reports";
    public static final String ZIP = "zip";
    public static final String LATEX = "latex";

    public static final Logger LOGGER = LogManager.getLogger("saflate-analysis");
    public static final int MAX_LOG_TEST_DETAILS = 10;
    public static final File TMP = new File(".tmp");

    public static final List<Reporter> reporters = new ArrayList<>();

    public static void main(String[] args) throws ParseException, IOException {

        Options options = new Options();
        options.addOption(ROOT_SURFIRE_REPORTS, true, "the folder containing subfolders or zip files with surefire reports");
        options.addOption(ZIP, false, "if set, look for reports in zip files instead of folders (optional)");
        options.addOption(LATEX,true,"name of a latex file summarising results (optional)");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse( options, args);

        if (!(cmd.hasOption(ROOT_SURFIRE_REPORTS))) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -cp <classpath> " + Main.class.getName(), "", options, "", true);
            System.exit(1);
        }

        boolean readReportsFromZips = cmd.hasOption(ZIP);
        LOGGER.info("Reading reports from " + (readReportsFromZips?"zip files":"subfolders"));

        if (readReportsFromZips) {
            if (TMP.exists()) {
                MoreFiles.deleteRecursively(TMP.toPath(), RecursiveDeleteOption.ALLOW_INSECURE);
            }
            TMP.mkdirs();
        }

        String latex = cmd.getOptionValue(LATEX);
        File latexOut = latex==null?null:new File(latex);
        if (latexOut!=null) {
            LOGGER.info("a latex summary will be created and written to " + latexOut.getAbsolutePath() );
            reporters.add(new LatexTableReporter(latexOut));
        }

        // initialise logging
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.INFO);

        LOGGER.info("loading surefire test reports");
        String rootFolderName = cmd.getOptionValue(ROOT_SURFIRE_REPORTS);
        File rootFolder = new File(rootFolderName);
        Preconditions.checkState(rootFolder.exists());
        Preconditions.checkState(rootFolder.isDirectory());

        List<TestRunResults> allResults = new ArrayList<>();

        reporters.stream().forEach(reporter -> reporter.start());

        Files.walk(rootFolder.toPath())
       //     .filter(Files::isRegularFile)
            .filter(path -> TestRunResults.isValidRunResult(path.toFile()))
            .forEach(path -> {
                File file = path.toFile();
                LOGGER.info("Including zip or folder to be parsed: " + file.toPath().toString());

                // if it is a zip file, simply unzip it into tmp .. quick and easy
                boolean proceed = false;
                if (readReportsFromZips && file.getName().endsWith(".zip")) {
                    File unzipped = new File(TMP,file.getName().substring(0,file.getName().length()-4));
                    ZipFile zipFile = new ZipFile(file);
                    LOGGER.info("\tunzipping " + zipFile.getFile().getName() + " to " + unzipped.getPath().toString());
                    try {
                        zipFile.extractAll(unzipped.getAbsolutePath());
                    }
                    catch (ZipException x) {
                        LOGGER.error("Error unzipping file",x);
                    }
                    file = unzipped;
                    proceed = true;
                }
                else if (file.isDirectory()){
                    proceed = true;
                }

                if (proceed) {
                    TestRunResults results = new TestRunResults(file);
                    allResults.add(results);
                }
        });
        LOGGER.info("" + allResults.size() + " results to be imported");

        // aggregation and reporting
        List<String> experiments = allResults.stream().map(r -> r.getExperimentName()).sorted().distinct().collect(Collectors.toList());
        List<String> datasets = allResults.stream().map(r -> r.getDataset()).sorted().distinct().collect(Collectors.toList());
        List<String> configurations = allResults.stream().map(r -> r.getConfiguration()).sorted().distinct().collect(Collectors.toList());
        boolean[] saflateOnOff = new boolean[]{true,false};

        for (String exp:experiments) {
            for (String dataset:datasets) {
                int runCounter = -1;
                for (String configuration:configurations) {
                    LOGGER.info("=======================================");
                    for (boolean saflateOn:saflateOnOff) {
                        LOGGER.info("Analysing results for experiment " + exp + ", dataset " + dataset + ", configuration " + configuration + ", saflate " + (saflateOn?"on":"off"));
                        List<TestRunResults> results = allResults.stream()
                            .filter(r -> r.getExperimentName().equals(exp))
                            .filter(r -> r.getDataset().equals(dataset))
                            .filter(r -> r.getConfiguration().equals(configuration))
                            .filter(r -> r.isUsesSaflate()==saflateOn)
                            .collect(Collectors.toList());
                        if (runCounter == -1) {
                            runCounter = results.size();
                        } else {
                            Preconditions.checkState(runCounter == results.size(), "inconsistent number of runs across configurations");
                        }

                        // iterate of multiple runs with the same parameters
                        List<Integer> failedTestCounts = new ArrayList<>();
                        List<Integer> succeededTestCounts = new ArrayList<>();
                        List<Integer> errorTestCounts = new ArrayList<>();
                        List<Integer> failedOrErrorTestCounts = new ArrayList<>();
                        List<Integer> skippedTestCounts = new ArrayList<>();
                        List<Integer> allTestCounts = new ArrayList<>();
                        List<Double> runtimes = new ArrayList<>();

                        for (TestRunResults result : results) {
                            LOGGER.debug("Parsing " + result.toString());
                            result.parseTestReports();
                            failedTestCounts.add((int) result.getTestResults().stream().filter(t -> t.getStatus() == TestCaseEvaluationResult.failure).count());
                            succeededTestCounts.add((int) result.getTestResults().stream().filter(t -> t.getStatus() == TestCaseEvaluationResult.success).count());
                            errorTestCounts.add((int) result.getTestResults().stream().filter(t -> t.getStatus() == TestCaseEvaluationResult.error).count());
                            skippedTestCounts.add((int) result.getTestResults().stream().filter(t -> t.getStatus() == TestCaseEvaluationResult.skip).count());
                            failedOrErrorTestCounts.add((int) result.getTestResults().stream().filter(t -> t.getStatus() == TestCaseEvaluationResult.failure || t.getStatus() == TestCaseEvaluationResult.error).count());
                            allTestCounts.add((int) result.getTestResults().stream().count());
                            runtimes.add(result.getRuntime());
                        }

                        Map<String,EnumSet<TestCaseEvaluationResult>> flakyTests = collectFlakyTests(results);
                        Map<String,EnumSet<TestCaseEvaluationResult>> stronglyFlakyTests = collectStronglyFlakyTests(results);

                        // aggregate and report
                        Stats failedTestStats = Stats.of(failedTestCounts);
                        Stats succeededTestStats = Stats.of(succeededTestCounts);
                        Stats errorTestStats = Stats.of(errorTestCounts);
                        Stats failedOrErrorTestStats = Stats.of(failedOrErrorTestCounts);
                        Stats skippedTestStats = Stats.of(skippedTestCounts);
                        Stats allTestStats = Stats.of(allTestCounts);
                        Stats runtimeStats = Stats.of(runtimes);

                        LOGGER.info("\t all tests: " + printStats(allTestStats));
                        LOGGER.info("\t successful tests: " + printStats(succeededTestStats));
                        LOGGER.info("\t failed tests: " + printStats(failedTestStats));
                        LOGGER.info("\t error tests: " + printStats(errorTestStats));
                        LOGGER.info("\t failed or error tests: " + printStats(failedOrErrorTestStats));
                        LOGGER.info("\t skipped tests: " + printStats(skippedTestStats));
                        LOGGER.info("\t flaky tests: " + flakyTests.size());
                        LOGGER.info("\t strongly flaky tests: " + stronglyFlakyTests.size());
                        LOGGER.info("\t runtimes (ms): " + printStats(runtimeStats));

                        Map<Reporter.ReportedDataKind,Object> data = new HashMap<>();
                        data.put(Reporter.ReportedDataKind.testCount,allTestStats);
                        data.put(Reporter.ReportedDataKind.succeeded,succeededTestStats);
                        data.put(Reporter.ReportedDataKind.failed,failedTestStats);
                        data.put(Reporter.ReportedDataKind.error,errorTestStats);
                        data.put(Reporter.ReportedDataKind.failOrError,failedOrErrorTestStats);
                        data.put(Reporter.ReportedDataKind.skipped,skippedTestStats);
                        data.put(Reporter.ReportedDataKind.flaky,""+flakyTests.size());
                        data.put(Reporter.ReportedDataKind.stronglyFlaky,""+stronglyFlakyTests.size());
                        data.put(Reporter.ReportedDataKind.runtime,runtimeStats);

                        reporters.forEach(reporter -> reporter.dataAcrossRuns(exp,dataset,configuration,saflateOn,data));

                        if (flakyTests.size()>0) {
                            LOGGER.info("\t\tflaky test details");
                            for (String ft : flakyTests.keySet()) {
                                LOGGER.info("\t\tflaky test detected: " + shorten(ft) + " -> " + flakyTests.get(ft) + ((isStronglyFlaky(flakyTests.get(ft))?" -- strongly flaky!":"")));
                            }
                        }
                    }
                }

                // running accuracy analysis if this is the network experiment
                boolean isNetworkExperiment = Objects.equals("network",exp) && Objects.equals(List.of("networkoff","networkon"),configurations);
                if (isNetworkExperiment) {
                    // analyseAccuracyOfNetworkExperiment(allResults,dataset,exp,configurations);
                    analyseRecallAndPrecision(allResults,dataset,exp,configurations);
                }
                else {
                    LOGGER.warn("Accuracy analysis not available for this experiment !");
                }
            }
        }
        reporters.stream().forEach(reporter -> reporter.finish());

    }


    // collect flaky tests across runs -- keys are test names, values are states
    private static Map<String,EnumSet<TestCaseEvaluationResult>> collectFlakyTests(List<TestRunResults> results) {
        Map<String,EnumSet<TestCaseEvaluationResult>> testStates = new HashMap<>();
        for (TestRunResults result:results) {
            for (TestCase test:result.getTestResults()) {
                String name = test.getFullName();
                EnumSet<TestCaseEvaluationResult> states = testStates.computeIfAbsent(name, n -> EnumSet.noneOf(TestCaseEvaluationResult.class));
                states.add(test.getStatus());
            }
        }

        Map<String,EnumSet<TestCaseEvaluationResult>> flakyTests = new HashMap<>();
        for (String name:testStates.keySet()) {
            EnumSet<TestCaseEvaluationResult> states = testStates.get(name);
            assert states.size()>0;
            if (states.size()>1) {
                flakyTests.put(name,states);
            }
        }

        return flakyTests;
    }

    // collect strongly flaky tests across runs -- keys are test names, values are states
    private static Map<String,EnumSet<TestCaseEvaluationResult>> collectStronglyFlakyTests(List<TestRunResults> results) {
        Map<String,EnumSet<TestCaseEvaluationResult>> testStates = new HashMap<>();
        for (TestRunResults result:results) {
            for (TestCase test:result.getTestResults()) {
                String name = test.getFullName();
                EnumSet<TestCaseEvaluationResult> states = testStates.computeIfAbsent(name, n -> EnumSet.noneOf(TestCaseEvaluationResult.class));
                states.add(test.getStatus());
            }
        }

        Map<String,EnumSet<TestCaseEvaluationResult>> stronglyflakyTests = new HashMap<>();
        for (String name:testStates.keySet()) {
            EnumSet<TestCaseEvaluationResult> states = testStates.get(name);
            assert states.size()>0;
            if (isStronglyFlaky(states)) {
                stronglyflakyTests.put(name,states);
            }
        }

        return stronglyflakyTests;
    }

    private static boolean isStronglyFlaky(EnumSet<TestCaseEvaluationResult> states) {
        return states.contains(TestCaseEvaluationResult.success) &&
                (states.contains(TestCaseEvaluationResult.failure) ||
                states.contains(TestCaseEvaluationResult.error));
    }


    private static void analyseRecallAndPrecision(List<TestRunResults> allResults,String dataset, String experiment, List<String> configurations) {
        LOGGER.info("Recall / precision analysis for experiment network " + experiment + ", dataset " + dataset);
        // relevant documents = tests succeeding with NW and fail/error without NW
        Set<String> testsAlwaysSucceedingWithSaflateOffAndNetworkOn = getTestsWithConsistentBehaviourAcrossRuns(
                allResults,
                r -> r.getExperimentName().equals(experiment) && r.getDataset().equals(dataset) && r.getConfiguration().equals("networkon") && !r.isUsesSaflate(),
                t -> t.getStatus() == TestCaseEvaluationResult.success
        );
        Set<String> testsAlwaysFailOrErrorWithSaflateOffAndNetworkOff = getTestsWithConsistentBehaviourAcrossRuns(
                allResults,
                r -> r.getExperimentName().equals(experiment) && r.getDataset().equals(dataset) && r.getConfiguration().equals("networkoff") && !r.isUsesSaflate(),
                t -> t.getStatus() == TestCaseEvaluationResult.error || t.getStatus() == TestCaseEvaluationResult.failure
        );
        Set<String> relevantTests = Sets.intersection(testsAlwaysSucceedingWithSaflateOffAndNetworkOn,testsAlwaysFailOrErrorWithSaflateOffAndNetworkOff);

        /// sanitised = retrieved
        Set<String> testsAlwaysSkippedWithSaflateOnAndNetworkOff = getTestsWithConsistentBehaviourAcrossRuns(
                allResults,
                r -> r.getExperimentName().equals(experiment) && r.getDataset().equals(dataset) && r.getConfiguration().equals("networkoff") && r.isUsesSaflate(),
                t -> t.getStatus() == TestCaseEvaluationResult.skip
        );
        Set<String> sanitisedTests = Sets.intersection(testsAlwaysFailOrErrorWithSaflateOffAndNetworkOff,testsAlwaysSkippedWithSaflateOnAndNetworkOff);
        Set<String> relevantAndSanitisedTests = Sets.intersection(relevantTests,sanitisedTests);

        // analyse spurious false positive rate, i.e. tests that consistently fail or error with network on and daflate off
        // and are then set to skip by saflate with network off
        // this indicates network-related problems already in the original built, main example: a database is not set up
        // or references servers are down
        Set<String> testsNeverSucceedingWithSaflateOffAndNetworkOn = getTestsWithConsistentBehaviourAcrossRuns (
                allResults,
                r -> r.getExperimentName().equals(experiment) && r.getDataset().equals(dataset) && r.getConfiguration().equals("networkon") && !r.isUsesSaflate(),
                t -> t.getStatus() != TestCaseEvaluationResult.success
        );

        Set<String> FPs = Sets.difference(sanitisedTests,relevantAndSanitisedTests);
        Set<String> FNs = Sets.difference(relevantTests,relevantAndSanitisedTests);

        Set<String> testsAlwaysSkippedWithSaflateOnAndNetworkOn = getTestsWithConsistentBehaviourAcrossRuns(
            allResults,
            r -> r.getExperimentName().equals(experiment) && r.getDataset().equals(dataset) && r.getConfiguration().equals("networkon") && r.isUsesSaflate(),
            t -> t.getStatus() == TestCaseEvaluationResult.skip
        );
        Set<String> spuriousFPsType1 = Sets.intersection(testsNeverSucceedingWithSaflateOffAndNetworkOn,testsAlwaysSkippedWithSaflateOnAndNetworkOn);

        Set<String> testsSometimesNotSucceedingWithSaflateOffAndNetworkOn = getTestsWithConsistentBehaviourAcrossRuns(
            allResults,
            r -> r.getExperimentName().equals(experiment) && r.getDataset().equals(dataset) && r.getConfiguration().equals("networkon") && !r.isUsesSaflate(),
            t -> t.getStatus() != TestCaseEvaluationResult.success
        );
        Set<String> spuriousFPsType2 = Sets.intersection(testsSometimesNotSucceedingWithSaflateOffAndNetworkOn,FPs);


        LOGGER.info("\t" + spuriousFPsType1.size() + " suspicious tests detected (tests never succeeding with networkon and saflateoff)");
        spuriousFPsType1.stream().limit(MAX_LOG_TEST_DETAILS).forEach(fp -> LOGGER.info("\t\tspurious FP detected: " + shorten(fp)));
        LOGGER.info("\t\t..");

        LOGGER.info("\t" + spuriousFPsType2.size() + " suspicious tests detected (tests sometimes not succeeding with networkon and saflateoff)");
        spuriousFPsType2.stream().limit(MAX_LOG_TEST_DETAILS).forEach(fp -> LOGGER.info("\t\tspurious FP detected: " + shorten(fp)));
        LOGGER.info("\t\t..");

        Set<String> testsFailOrErrorWithSaflateOffAndNetworkOffEncounteringNetworkExceptions = getTestsSometimesShowingBehaviourAcrossRuns(
            allResults,
            r -> r.getExperimentName().equals(experiment) && r.getDataset().equals(dataset) && r.getConfiguration().equals("networkoff") && !r.isUsesSaflate(),
            t -> t.getStatus() == TestCaseEvaluationResult.failure || t.getStatus() == TestCaseEvaluationResult.error
        );

        Set<String> testsFailOrErrorWithSaflateOffAndNetworkOnEncounteringNetworkExceptions = getTestsSometimesShowingBehaviourAcrossRuns(
                allResults,
                r -> r.getExperimentName().equals(experiment) && r.getDataset().equals(dataset) && r.getConfiguration().equals("networkon") && !r.isUsesSaflate(),
                t -> t.getStatus() == TestCaseEvaluationResult.failure || t.getStatus() == TestCaseEvaluationResult.error
        );

        LOGGER.info("\t" + FNs.size() + " FNs detected");
        FNs.stream().limit(MAX_LOG_TEST_DETAILS).forEach(fn -> LOGGER.info("\t\tFN detected: " + shorten(fn)));
        LOGGER.info("\t\t..");

        LOGGER.info("\t" + FPs.size() + " FPs detected");
        FPs.stream().limit(MAX_LOG_TEST_DETAILS).forEach(fp -> LOGGER.info("\t\tFP detected: " + shorten(fp)));
        LOGGER.info("\t\t..");

        double precision = ((double)relevantAndSanitisedTests.size()) / ((double)sanitisedTests.size());
        double recall = ((double)relevantAndSanitisedTests.size()) / ((double)relevantTests.size());
        double spurious = ((double)Sets.intersection(sanitisedTests,spuriousFPsType1).size()) / ((double)relevantTests.size());

        Map<Reporter.ReportedDataKind,Object> data = new HashMap<>();
        data.put(Reporter.ReportedDataKind.precision,precision);
        data.put(Reporter.ReportedDataKind.recall,recall);
        reporters.forEach(reporter -> reporter.summaryStats(experiment,dataset,data));

        LOGGER.info("\tprecision = " + precision);
        LOGGER.info("\trecall = " + recall);

    }

    // avoid page breaks
    private static String shorten(String testName) {
        if (testName.contains("]")) {
            return testName.substring(0,testName.indexOf("]")+1);
        }
        return testName;
    }

    private static String printStats(Stats stats) {
        if (stats.min()==stats.max()) {
            return "" + stats.mean()  + " (no variation), count= " + stats.count();
        }
        else {
            return "avg=" + stats.mean() + " min=" + stats.min() + " max=" + stats.max() + " stdev=" + stats.populationStandardDeviation() + " count=" + stats.count();
        }
    }

    private static Set<String> getTestsWithConsistentBehaviourAcrossRuns(List<TestRunResults> allResults, Predicate<TestRunResults> runFilter,Predicate<TestCase> testFilter) {
        Set<String> tests = null;
        List<TestRunResults> results = allResults.stream()
            .filter(runFilter)
            .collect(Collectors.toList());
        for (TestRunResults result : results) {
            // this is one single run
            Set<String> tests2 = result.getTestResults().stream()
                .filter(testFilter)
                .map(t -> t.getFullName())
                .collect(Collectors.toSet());
            if (tests==null) {
                tests = tests2;
            }
            else {
                tests = Sets.intersection(tests,tests2);
            }
        }
        return tests;
    }

    private static Set<String> getTestsSometimesShowingBehaviourAcrossRuns(List<TestRunResults> allResults, Predicate<TestRunResults> runFilter,Predicate<TestCase> testFilter) {
        Set<String> tests = new HashSet<>();
        List<TestRunResults> results = allResults.stream()
                .filter(runFilter)
                .collect(Collectors.toList());
        for (TestRunResults result : results) {
            // this is one single run
            Set<String> tests2 = result.getTestResults().stream()
                .filter(testFilter)
                .map(t -> t.getFullName())
                .collect(Collectors.toSet());
            tests.addAll(tests2);
        }
        return tests;
    }

    private static boolean isErrorOrFailureCauseByNetworkException (TestCase t){
        if (t.getStatus() != TestCaseEvaluationResult.success && t.getDetails()!=null) {
            return  t.getDetails().contains("java.net.UnknownHostException") ||
                    t.getDetails().contains("java.net.SocketException") ||
                    t.getDetails().contains("java.net.NoRouteToHostException");
        }
        else {
            return false;
        }
    }

}
