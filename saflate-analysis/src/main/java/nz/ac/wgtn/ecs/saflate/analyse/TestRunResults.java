package nz.ac.wgtn.ecs.saflate.analyse;

import com.google.common.base.Preconditions;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Represents run results.
 * @author jens dietrich
 */
public class TestRunResults {

    public static final Pattern REPORT_FOLDER_PATTERN = Pattern.compile("testresults-(.*)-((saflateoff)|(saflateon))-\\d+");
    public static final Pattern REPORT_ZIP_PATTERN = Pattern.compile("testresults-(.*)-((saflateoff)|(saflateon))-\\d+\\.zip");

    // instance creation should be guarded by this
    static boolean isValidRunResult (File file) {
        Preconditions.checkState(file!=null);
        if (file.isDirectory()){
            return REPORT_FOLDER_PATTERN.matcher(file.getName()).matches();
        }
        else {
            return REPORT_ZIP_PATTERN.matcher(file.getName()).matches();
        }
    }

    public TestRunResults(File file) {
        Preconditions.checkState(isValidRunResult(file),"this is not a valid file containing experiment results");
        String[] tokens = file.getName().split("-");

        this.experimentName = tokens[1];
        this.dataset = tokens[2];
        int configurationTokenIndex = tokens.length-3;
        // this is to account for "-" in program names
        for (int i=3;i<configurationTokenIndex;i++) {
            this.dataset = this.dataset + "-" + tokens[i];
        }
        this.configuration = tokens[configurationTokenIndex];
        this.usesSaflate = "saflateon".equals(tokens[configurationTokenIndex+1]);
        this.counter = Integer.parseInt(tokens[configurationTokenIndex+2]);
        this.reports = file;
    }

    // example: network, environ
    private String experimentName = null;
    private File reports = null;
    private int counter = 0;
    private String dataset = null;
    // example: networkon, networkoff
    private String configuration = null;
    private boolean usesSaflate = false;

    private List<TestSuite> testSuites = null;

    public void parseTestReports() throws IOException {
        testSuites = SurefireReportParser.parse(this.reports);
    }

    public List<TestSuite> getTestSuites() {
        // run parseTestReports first
        Preconditions.checkState(testSuites!=null,"test suites not initialised");
        return testSuites;
    }

    public double getRuntime () {
        // run parseTestReports first
        Preconditions.checkState(testSuites!=null,"test suites not initialised");
        return this.testSuites.stream().mapToDouble(ts -> ts.getTime()).sum();
    }

    public List<TestCase> getTestResults() {
        return getTestSuites().stream()
            .flatMap(ts -> ts.getTestCases().stream())
            .collect(Collectors.toList());
    }

    public String getDataset() {
        return dataset;
    }

    public void setDataset(String dataset) {
        this.dataset = dataset;
    }

    public String getExperimentName() {
        return experimentName;
    }

    public void setExperimentName(String experimentName) {
        this.experimentName = experimentName;
    }

    public File getReports() {
        return reports;
    }

    public void setReports(File reports) {
        this.reports = reports;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getConfiguration() {
        return configuration;
    }

    public void setConfiguration(String configuration) {
        this.configuration = configuration;
    }

    public boolean isUsesSaflate() {
        return usesSaflate;
    }

    public void setUsesSaflate(boolean usesSaflate) {
        this.usesSaflate = usesSaflate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestRunResults that = (TestRunResults) o;
        return counter == that.counter && usesSaflate == that.usesSaflate && Objects.equals(experimentName, that.experimentName) && Objects.equals(reports, that.reports) && Objects.equals(dataset, that.dataset) && Objects.equals(configuration, that.configuration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(experimentName, reports, counter, dataset, configuration, usesSaflate);
    }

    @Override
    public String toString() {
        return "[" +
                "experiment='" + experimentName + '\'' +
                ", dataset='" + dataset + '\'' +
                ", configuration='" + configuration + '\'' +
                ", saflate=" + (usesSaflate?"on":"off")+
                ", run=" + counter +
                ']';
    }
}
