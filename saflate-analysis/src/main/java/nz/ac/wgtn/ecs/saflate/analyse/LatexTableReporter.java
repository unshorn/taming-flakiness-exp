package nz.ac.wgtn.ecs.saflate.analyse;

import com.google.common.math.DoubleMath;
import com.google.common.math.Stats;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * Listener to report results.
 * @author jens dietrich
 */
public class LatexTableReporter implements Reporter {

    private static NumberFormat DOUBLE_FORMAT1 = new DecimalFormat("#0.0");
    private static NumberFormat DOUBLE_FORMAT2 = new DecimalFormat("#0.00");
    private static NumberFormat INT_FORMAT = NumberFormat.getNumberInstance(Locale.US);


    private File target = null;


    private List<String> datasets = new ArrayList<>();
    private Map<Key,String> results = new HashMap<>();

    public LatexTableReporter(File target) {
        this.target = target;
    }

    static class Key {
        private String dataset = null;
        private String configuration = null;
        private boolean saflate = false;
        private ReportedDataKind dataKind = null;

        public Key(String dataset, String configuration, boolean saflate, ReportedDataKind dataKind) {
            this.dataset = dataset;
            this.configuration = configuration;
            this.saflate = saflate;
            this.dataKind = dataKind;
        }

        public Key(String dataset, ReportedDataKind dataKind) {
            this.dataset = dataset;
            this.dataKind = dataKind;
        }

        public String getDataset() {
            return dataset;
        }

        public String getConfiguration() {
            return configuration;
        }

        public boolean isSaflate() {
            return saflate;
        }

        public ReportedDataKind getDataKind() {
            return dataKind;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Key key = (Key) o;
            return saflate == key.saflate && Objects.equals(dataset, key.dataset) && Objects.equals(configuration, key.configuration) && dataKind == key.dataKind;
        }

        @Override
        public int hashCode() {
            return Objects.hash(dataset, configuration, saflate, dataKind);
        }
    }

    @Override
    public void start() {
    /*    try (PrintWriter out = new PrintWriter(new FileWriter(target))) {
            out.println("\\begin{table*}[]");
            out.println("\\resizebox{\\textwidth}{!}{");
            out.println("\t\\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|}");
            out.println("\t\t\\hline");
            out.println("\t\t\\multirow{3}{*}{prog.} & \\multirow{3}{*}{tests} & \\multicolumn{12}{c|}{unsanitised} & \\multicolumn{12}{c|}{sanitised} & \\multirow{3}{*}{prec.} & \\multirow{3}{*}{recall} \\\\ \\cline{3-26}");
            out.println("\t\t&  & \\multicolumn{6}{c|}{network on} & \\multicolumn{6}{c|}{network off} & \\multicolumn{6}{c|}{network on}    & \\multicolumn{6}{c|}{network off} & & \\\\ \\cline{3-26}");
            out.println("\t\t\t\t& & fail  & err    & skp & flk & sflk & rt (s) & fail & err   & skp  & flk & sflk  & rt (s)  & fail & err   & skp     & flk & sflk & rt (s)  & fail & err   & skp    & flk & sflk & rt (s) &  & \\\\ \\hline");
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void finish() {
        try (PrintWriter out = new PrintWriter(new FileWriter(target))) {
            // TABLE 1
            out.println("\\begin{table*}[]");
            out.println("\t\\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|}");
            out.println("\t\t\\hline");
            out.println("\t\t\\multirow{3}{*}{program} & \\multirow{3}{*}{tests} & \\multicolumn{12}{c|}{unsanitised}  \\\\ \\cline{3-14}");
            out.println("\t\t&  & \\multicolumn{6}{c|}{network on} & \\multicolumn{6}{c|}{network off}  \\\\ \\cline{3-14}");
            out.println("\t\t\t\t& & failure  & error    & skipped & flaky & strongly flaky & rt (s) & failed & error   & skipped  & flaky & strongly flaky  & rt (s)   \\\\ \\hline");
            // print rows

            for (int i=0;i<datasets.size();i++) {
                String dataset = datasets.get(i);
                out.print(dataset);
                out.print(" & ");
                out.print(results.get(new Key(dataset,"networkon",false,ReportedDataKind.testCount)));

                printDetailedResults(out,dataset,"networkon",false);
                printDetailedResults(out,dataset,"networkoff",false);


                out.print(" \\\\ ");

                // last row:
                if (i==datasets.size()-1) {
                    out.print(" \\hline ");
                }
                out.println();

            }

            // finish
            out.println("\t\\end{tabular}");
            out.println("\\caption{Test results without sanitisation, the numbers reported are the tests resulting in failure,  error or  skipped. Tests are reported across 10 test runs, numbers in brackets means that we observed some variation across runs and report the mean. The number of flaky and strongly flaky tests across those runs are reported as well.  Runtimes are reported in seconds (rt(s)).}");
            out.println("\\label{tab:unsanitized}");
            out.println("\\end{table*}");


// TABLE 2
            out.println("\\begin{table*}[]");
            out.println("\t\\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|}");
            out.println("\t\t\\hline");
            out.println("\t\t\\multirow{3}{*}{prog.} & \\multirow{3}{*}{tests} & \\multicolumn{12}{c|}{sanitised}  \\\\ \\cline{3-14}");
            out.println("\t\t&  & \\multicolumn{6}{c|}{network on} & \\multicolumn{6}{c|}{network off}  \\\\ \\cline{3-14}");
            out.println("\t\t\t\t& & failure  & error    & skipped & flaky & strongly flaky & rt (s) & failed & error   & skipped  & flaky & strongly flaky  & rt (s)   \\\\ \\hline");
            // print rows

            for (int i=0;i<datasets.size();i++) {
                String dataset = datasets.get(i);
                out.print(dataset);
                out.print(" & ");
                out.print(results.get(new Key(dataset,"networkon",false,ReportedDataKind.testCount)));


                printDetailedResults(out,dataset,"networkon",true);
                printDetailedResults(out,dataset,"networkoff",true);

                // out.print(" & ");
                // out.print(results.get(new Key(dataset,ReportedDataKind.precision)));

                // out.print(" & ");
                //  out.print(results.get(new Key(dataset,ReportedDataKind.recall)));

                out.print(" \\\\ ");

                // last row:
                if (i==datasets.size()-1) {
                    out.print(" \\hline ");
                }
                out.println();

            }

            // finish
            out.println("\t\\end{tabular}");
            out.println("\\caption{Test results with sanitisation.}");
            out.println("\\label{tab:sanitized}");
            out.println("\\end{table*}");

            // TABLE 3

            out.println("\\begin{table}[]");
            out.println("\\center");
            out.println("\t\\begin{tabular}{|l|l|l|}");
            out.println("\t\t\\hline");
            out.println("\t\t\t program & precision & recall \\\\ \\hline");
            // print rows

            for (int i=0;i<datasets.size();i++) {
                String dataset = datasets.get(i);
                out.print(dataset);
                out.print(" & ");

                out.print(results.get(new Key(dataset,ReportedDataKind.precision)));

                out.print(" & ");
                out.print(results.get(new Key(dataset,ReportedDataKind.recall)));

                out.print(" \\\\ ");

                // last row:
                if (i==datasets.size()-1) {
                    out.print(" \\hline ");
                }
                out.println();

            }

            // finish
            out.println("\t\\end{tabular}");
            out.println("\\caption{Precision and recall for sanitization.}");
            out.println("\\label{tab:precrecall}");
            out.println("\\end{table}");


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printDetailedResults(PrintWriter out, String dataset,String configuration,boolean saflateOn) throws IOException {
        out.print(" & ");
        out.print(results.get(new Key(dataset,configuration,saflateOn,ReportedDataKind.failed)));

        out.print(" & ");
        out.print(results.get(new Key(dataset,configuration,saflateOn,ReportedDataKind.error)));

        out.print(" & ");
        out.print(results.get(new Key(dataset,configuration,saflateOn,ReportedDataKind.skipped)));

        out.print(" & ");
        out.print(results.get(new Key(dataset,configuration,saflateOn,ReportedDataKind.flaky)));

        out.print(" & ");
        out.print(results.get(new Key(dataset,configuration,saflateOn,ReportedDataKind.stronglyFlaky)));

        out.print(" & ");
        out.print(results.get(new Key(dataset,configuration,saflateOn,ReportedDataKind.runtime)));
    }


    @Override
    public void dataAcrossRuns(String experiment,String dataset,String configuration,boolean saflateOn,Map<ReportedDataKind, Object> data) {
        if (Objects.equals("network",experiment)) {
            if (!datasets.contains(dataset)) {
                datasets.add(dataset);
            }
            for (ReportedDataKind dataKind:data.keySet()) {
                Key key = new Key(dataset, configuration, saflateOn,dataKind);
                Object value = data.get(dataKind);
                String result = value2string(value,false);
                results.put(key, result);
            }
        }
        else {
            Main.LOGGER.warn("results not processed for experiment " + experiment);
        }
    }

    @Override
    public void summaryStats(String experiment, String dataset, Map<ReportedDataKind, Object> data) {
        if (Objects.equals("network",experiment)) {
            assert (datasets.contains(dataset));
            for (ReportedDataKind dataKind:data.keySet()) {
                Key key = new Key(dataset,dataKind);
                Object value = data.get(dataKind);
                String result = value2string(value,true);
                results.put(key, result);
            }
        }
        else {
            Main.LOGGER.warn("results not processed for experiment " + experiment);
        }
    }

    private static String value2string(Object value,boolean highPrecisionForDoubles) {
        if (value instanceof Stats) {
            return stats2string((Stats)value);
        }
        else if (value instanceof Double) {
            return highPrecisionForDoubles?double2string2((Double)value):double2string1((Double)value);
        }
        else if (value instanceof Integer) {
            return int2string((Integer)value);
        }
        else {
            return ""+value;
        }
    }

    private static String stats2string(Stats stats) {
        if (stats.min()==stats.max()) {
            return double2string1(stats.mean());
        }
        else {
            return "(" + double2string1(stats.mean()) + ")";
        }
    }

    private static String double2string1(double value) {
        if (DoubleMath.isMathematicalInteger(value)) {
            return INT_FORMAT.format((int)value);
        }
        else {
            return DOUBLE_FORMAT1.format(value);
        }
    }

    private static String double2string2(double value) {
        if (DoubleMath.isMathematicalInteger(value)) {
            return INT_FORMAT.format((int)value);
        }
        else {
            return DOUBLE_FORMAT2.format(value);
        }
    }

    private static String int2string(int value) {
        return INT_FORMAT.format(value);
    }
}
