package nz.ac.wgtn.ecs.saflate.analyse;

/**
 * Represents the result of evaluating a test.
 * Surefires own flakiness handling is not supported.
 * @author jens dietrich
 */
public enum TestCaseEvaluationResult {
    failure,error,success,skip
}
