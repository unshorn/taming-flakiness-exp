package nz.ac.wgtn.ecs.saflate.analyse;

import java.util.Objects;

/**
 * Represents a test case and its evaluation status.
 * Part of a simplified domain model defined by https://maven.apache.org/surefire/maven-surefire-plugin/xsd/surefire-test-report.xsd .
 * @author jens dietrich
 */
public class TestCase {
    private String name = null;
    private String className = null;
    private TestCaseEvaluationResult status = TestCaseEvaluationResult.success;
    private String details = null;

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public TestCaseEvaluationResult getStatus() {
        return status;
    }

    public void setStatus(TestCaseEvaluationResult status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestCase testCase = (TestCase) o;
        return Objects.equals(name, testCase.name) && Objects.equals(className, testCase.className) && status == testCase.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, className, status);
    }

    public String getFullName() {
        return "" + className + "::" + name;
    }

    @Override
    public String toString() {
        return "testcase " + className + "::" + name + " -> " + status.name();
    }
}
