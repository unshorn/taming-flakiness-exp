## README 

This project contains a script to parse experiment results (folders or zip files containing surefilre reports), and output a summary 
of results including runtimes and precision/false positive and recall/false negative analysis to the console. 

```
usage: java -cp <classpath> nz.ac.wgtn.ecs.saflate.analyse.Main [-reports <arg>] [-zip]

-reports <arg>      the folder containing subfolders or zip files with surefire reports
-zip                if set, look for reports in zip files instead of folders (optional)
-latex <arg>        if set, a latex file with a name set by the arg will be created (optional)
```

The classpath can be computed with `mvn dependency:build-classpath`. 

Folders or files must have names following the following pattern to be considered: `testresults-<experimenttype>-<program>-<configuration>-<saflateoff|saflateoff>-counter[.zip]`. 

Or, as regular expression:

`testresults-(\\w+)-(\\w+)-(\\w+)-((saflateoff)|(saflateon))-\\d+"[.zip]`

Example: `testresults-network-pdfbox-networkon-saflateon-1.zip`

### Analysing Classifier Rules

*saflate* contains a component to learn environmental dependencies from surefire reports, using the weka JRip classifier.

The script `nz.ac.wgtn.ecs.saflate.analyse.AnalyseJRipRules` can be used to analyse rule sets, extracting some 
simple characteristics like target classification and number of prerequisites. It expects a single parameter `-rules` pointing to
a folder that has rule definitions (files ending with `-rules.txt`).